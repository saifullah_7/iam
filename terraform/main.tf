module "ec2" {
  source = "./module/ec2"
}

module "iam" {
  source = "./module/iam"
}

module "user" {
  source = "./module/user"
}