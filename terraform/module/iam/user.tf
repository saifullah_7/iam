resource "aws_iam_user" "new_user" {
    name= var.myname
}


resource "aws_iam_user_policy" "cloudwatch_policy" {
  name = "cloudwatch_policy"
  policy = "${file("module/iam/policy/user_policy.json")}"
  user = "${aws_iam_user.new_user.name}"
}