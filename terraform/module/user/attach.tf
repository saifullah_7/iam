resource "aws_iam_policy" "ec2_read" {
  name = "ec2_read"
  policy = "${file("module/user/policy/user_console_policy.json")}"
}